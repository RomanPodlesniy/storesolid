using ClassLibrary1;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace TestProject1
{
    [TestFixture]
    public class Tests
    {
        const string AssemblyName = "ClassLibrary1";

        private void AssertFailIfNull(object obj, string message)
        {
            if (obj == null)
            {
                Assert.Fail($"{message} doesn't exist");
            }
        }

        private Type GetCustomType(string name, string message)
        {
            var type = Type.GetType($"{AssemblyName}.{name}, {AssemblyName}");
            AssertFailIfNull(type, message);
            return type;
        }

        [Test]
        public void User_IsAbstractClassOrExists()
        {
            var type = Type.GetType($"{AssemblyName}.User, {AssemblyName}");

            AssertFailIfNull(type, "Class 'User'");

            if (!type.IsAbstract)
            {
                Assert.Fail("Class 'User' is not abstract.");
            }
        }

        [TestCase("User")]
        [TestCase("UserList")]
        [TestCase("RegisteredUser")]
        [TestCase("Guest")]
        [TestCase("Admin")]
        [TestCase("Order")]
        [TestCase("OrderList")]
        [TestCase("Product")]
        [TestCase("ProductList")]
        [TestCase("Engine")]
        [TestCase("Utilities")]

        public void Class_IsClassExists(string className)
        {
            var type = Type.GetType($"{AssemblyName}.{className}, {AssemblyName}");

            AssertFailIfNull(type, $"Class '{className}'");
        }

        [TestCase("User", "Guest")]
        [TestCase("User", "RegisteredUser")]
        [TestCase("User", "Admin")]
        public void Class_IsClassInheritsFromClass(string parentClassName, string childClassName)
        {
            var parentType = Type.GetType($"{AssemblyName}.{parentClassName}, {AssemblyName}");
            var childType = Type.GetType($"{AssemblyName}.{childClassName}, {AssemblyName}");

            AssertFailIfNull(parentType, $"Class '{parentClassName}'");
            AssertFailIfNull(childType, $"Class '{childClassName}'");

            if (!parentType.IsAbstract && !childType.IsSubclassOf(parentType))
            {
                Assert.Fail($"Class '{childType}' doesn't inherit from '{parentType}'");
            }
        }
        [TestCase("User", "DisplayUserData", typeof(void))]
        [TestCase("User", "ShowUserStatus", typeof(void))]
        [TestCase("User", "ShowStartMenue", typeof(void))]

        [TestCase("Guest", "Registration", typeof(RegisteredUser))]
        [TestCase("Guest", "DisplayUserData", typeof(void))]
        [TestCase("Guest", "ShowUserStatus", typeof(void))]
        [TestCase("Guest", "ShowStartMenue", typeof(void))]
        [TestCase("Guest", "PromptToLogIntoAccaunt", typeof(void))]

        [TestCase("RegisteredUser", "ShowUserStatus", typeof(void))]
        [TestCase("RegisteredUser", "ShowStartMenue", typeof(void))]
        [TestCase("RegisteredUser", "ChangePersonalInformation", typeof(void))]
        [TestCase("RegisteredUser", "SetStatusOfOrderAsReceived", typeof(void))]
        [TestCase("RegisteredUser", "ViewHistoryOfOrdersAndItStatus", typeof(void))]
        [TestCase("RegisteredUser", "CreateNewOrder", typeof(void))]
        [TestCase("RegisteredUser", "CancellationOfOrder", typeof(void))]

        [TestCase("Admin", "ShowUserStatus", typeof(void))]
        [TestCase("Admin", "ShowStartMenue", typeof(void))]
        [TestCase("Admin", "ChangePersonalInformation", typeof(void))]
        [TestCase("Admin", "ChangeTheStatusOfTheOrder", typeof(void))]
        [TestCase("Admin", "ViewHistoryOfOrdersAndItStatus", typeof(void))]
        [TestCase("Admin", "CreateNewOrder", typeof(void))]
        [TestCase("Admin", "CancellationOfOrder", typeof(void))]
        [TestCase("Admin", "Ordering", typeof(void))]
        [TestCase("Admin", "AddNewProduct", typeof(void))]
        [TestCase("Admin", "ChangeInformationAboutTheProduct", typeof(void))]
        [TestCase("Admin", "Ordering", typeof(void))]

        [TestCase("UserList", "AddUser", typeof(void))]
        [TestCase("UserList", "RemoveUser", typeof(void))]

        [TestCase("Order", "Display", typeof(void))]

        [TestCase("OrderList", "AddOrder", typeof(void))]
        [TestCase("OrderList", "RemoveOrder", typeof(void))]
        [TestCase("OrderList", "FindOrder", typeof(Order))]

        [TestCase("Product", "Display", typeof(void))]
        [TestCase("ProductList", "AddProduct", typeof(void))]

        [TestCase("Engine", "MenueForGuest", typeof(void))]
        [TestCase("Engine", "MenueForRegisteredUsers", typeof(void))]
        [TestCase("Engine", "MenueForAdmin", typeof(void))]
        [TestCase("Engine", "LogOutFromAccaunt", typeof(void))]

        [TestCase("Utilities", "ShowProgramTitle", typeof(void))]
        [TestCase("Utilities", "SearchProductByName", typeof(Product))]
        [TestCase("Utilities", "ViewListOfProducts", typeof(void))]
        [TestCase("Utilities", "StatusOfUserCheking", typeof(string))]
        [TestCase("Utilities", "FindUser", typeof(RegisteredUser))]
        [TestCase("Utilities", "PersonalData", typeof(Tuple<string, string, string, string, string, string, string>))]
        [TestCase("Utilities", "DataForCreationInstance", typeof(void))]
        [TestCase("Utilities", "PromptToPressAnyKey", typeof(void))]


        public void Class_HasMethodsWithCorrectSignatures(string className, string methodName, Type returnType)
        {
            var classType = Type.GetType($"{AssemblyName}.{className}, {AssemblyName}");
            AssertFailIfNull(classType, $"Class '{className}'");

            var methodType = classType.GetMethod(methodName);
            AssertFailIfNull(methodType, $"Method '{methodName}'");

            if (methodType.ReturnType != returnType)
            {
                Assert.Fail($"Class '{className}' doesn't have method with name '{methodName}' that returns '{returnType}'.");
            }
        }

        [Test]
        public void User_HasAbstractMethodIncome()
        {
            var classType = Type.GetType($"{AssemblyName}.User, {AssemblyName}");
            AssertFailIfNull(classType, $"Class 'User'");

            var methodType1 = classType.GetMethod("ShowUserStatus");
            var methodType2 = classType.GetMethod("ShowStartMenue");

            AssertFailIfNull(methodType1, $"Method 'ShowUserStatus'");
            AssertFailIfNull(methodType2, $"Method 'ShowStartMenue'");

            if (!methodType1.IsAbstract)
            {
                Assert.Fail("Method 'ShowUserStatus' is not abstract.");
            }

            if (!methodType2.IsAbstract)
            {
                Assert.Fail("Method 'ShowStartMenue' is not abstract.");
            }
        }

        [TestCase("User", typeof(string), typeof(string), typeof(string), typeof(string), typeof(string), typeof(string), typeof(string), typeof(string))]
        [TestCase("Guest")]
        [TestCase("RegisteredUser", typeof(string), typeof(string), typeof(string), typeof(string), typeof(string), typeof(string), typeof(string), typeof(string))]
        [TestCase("Admin", typeof(string), typeof(string), typeof(string), typeof(string), typeof(string), typeof(string), typeof(string), typeof(string))]
        [TestCase("Product", typeof(string), typeof(string), typeof(string), typeof(decimal), typeof(int))]
        public void Class_DoesHaveConstructor(string className, params Type[] parameters)
        {
            var type = Type.GetType($"{AssemblyName}.{className}, {AssemblyName}");

            AssertFailIfNull(type, $"Class '{className}'");

            var constructor = type.GetConstructor(parameters);

            if (constructor == null && !constructor.IsPublic)
            {
                Assert.Fail($"Class {className} has invalid constructor.");
            }
        }


        [TestCase("UserList", typeof(IEnumerable<>), "User")]
        [TestCase("ProductList", typeof(IEnumerable<>), "Product")]
        [TestCase("OrderList", typeof(IEnumerable<>), "Order")]

        public void Class_ImplementsLibraryGenericInterface(string className, Type interfaceType,
            string genericClassName)
        {
            var classType = Type.GetType($"{AssemblyName}.{className}, {AssemblyName}");
            var genericType = Type.GetType($"{AssemblyName}.{genericClassName}, {AssemblyName}");

            if (classType == null || !classType.IsClass)
            {
                Assert.Fail($"Class '{className}' doesn't exist");
            }

            if (genericType == null || !classType.IsClass)
            {
                Assert.Fail($"Class '{genericClassName}' doesn't exist");
            }

            var genericInterfaceType = interfaceType.MakeGenericType(genericType);

            if (!classType.GetInterfaces().Contains(genericInterfaceType))
            {
                Assert.Fail($"Class '{className}' doesn't implement interface '{genericInterfaceType.Name}'.");
            }
        }





    }
}